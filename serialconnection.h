#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include "types.h"
#include "settingsdialog.h"

#include <QTimer>
#include <QObject>
#include <QtSerialPort/QSerialPort>

enum class BinarySendState
{
    PREAMBLE,
    CHUNK,
    END,
    NONE
};

class SerialConnection : public QObject
{
    Q_OBJECT

public:
    explicit SerialConnection();
    virtual ~SerialConnection();

    void sendCommand(uchar cmd);
    void sendBinary(QString path);
    void sendNewName(QString name);
    void sendNewPassword(QString password);
    void sendBluetoothMac();
    void sendStatusRequest();

signals:
    void portOpened();
    void portClosed();
    void startBinarySend();
    void stopBinarySend();
    void dataSended(QString);
    void dataReceived(QString);
    void printInputType(QString);
    void printOutputType(QString);

public slots:
    void openPort();
    void closePort();
    void readData();
    void openSettings();

private slots:
    void timeoutHandler();
    void writeData(const QByteArray &data);

private:
    QSerialPort* m_serial;
    SettingsDialog* m_settings;

    QByteArray m_data;
    QTimer* m_responseTimer;

    QByteArray m_binary;
    uint32_t m_binaryChunkCounter;
    const uint16_t m_chunkSize = 56;
    BinarySendState m_binaryState = BinarySendState::NONE;

    QString packageToString(const QByteArray &data);
    void sendPackage(OutputPackage type, const QByteArray &data = QByteArray());

    void sendBinaryEnd();
    void sendBinaryChunk();
    void sendBinaryPreamble(QString name, uint16_t binaryCRC);

    bool isValidInput() const;
    void checkBinarySendState();

    template <typename T>
    void addValToPackage(const T& val, QByteArray &package)
    {
        for (size_t i = 0; i < sizeof(val); ++i) {
            package.append((uchar)((val & (0xFF << (i * 8))) >> (i * 8)));
        }
    }
};

#endif // SERIALCONNECTION_H
