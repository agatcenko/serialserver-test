#ifndef CRC_H
#define CRC_H

unsigned short updateCRC(unsigned short acc, const unsigned char input);
unsigned short calculateCRC(const unsigned char* data, const unsigned long long len);

#endif // CRC_H
