#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "terminal.h"
#include "serialconnection.h"

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void sendRunCommand();
    void sendKillCommand();
    void sendNewName();
    void sendNewPassword();
    void sendBinary();
    void chooseBinary();
    void sendStatusRequest();

    void enableActionBox();
    void disableActionBox();
    void showInput(QString data);
    void showOutput(QString data);

private:
    Ui::MainWindow *ui;

    Terminal* m_inTerminal;
    Terminal* m_outTerminal;
    SerialConnection* m_connection;

    QString m_binaryPath;
};

#endif // MAINWINDOW_H
