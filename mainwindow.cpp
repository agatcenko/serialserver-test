#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_inTerminal = new Terminal;
    ui->inputConsole->addWidget(m_inTerminal);

    m_outTerminal = new Terminal;
    ui->outputConsole->addWidget(m_outTerminal);

    ui->actionBox->setDisabled(true);
    ui->statusBox->setDisabled(true);

    m_connection = new SerialConnection;

    connect(ui->actionClose, &QAction::triggered, this, &MainWindow::close);
    connect(ui->actionConnect, &QAction::triggered, m_connection, &SerialConnection::openPort);
    connect(ui->actionDisconnect, &QAction::triggered, m_connection, &SerialConnection::closePort);
    connect(ui->actionConfigure, &QAction::triggered, m_connection, &SerialConnection::openSettings);

    connect(ui->runCmdBtn, SIGNAL(clicked(bool)), this, SLOT(sendRunCommand()));
    connect(ui->killCmdBtn, SIGNAL(clicked(bool)), this, SLOT(sendKillCommand()));

    connect(ui->nameBtn, SIGNAL(clicked(bool)), this, SLOT(sendNewName()));
    connect(ui->passwordBtn, SIGNAL(clicked(bool)), this, SLOT(sendNewPassword()));

    connect(ui->binarySendBtn, SIGNAL(clicked(bool)), this, SLOT(sendBinary()));
    connect(ui->binaryOpenBtn, SIGNAL(clicked(bool)), this, SLOT(chooseBinary()));

    connect(ui->statusReqBtn, SIGNAL(clicked(bool)), this, SLOT(sendStatusRequest()));

    connect(m_connection, SIGNAL(portOpened()), this, SLOT(enableActionBox()));
    connect(m_connection, SIGNAL(portClosed()), this, SLOT(disableActionBox()));

    connect(m_connection, SIGNAL(startBinarySend()), this, SLOT(disableActionBox()));
    connect(m_connection, SIGNAL(stopBinarySend()), this, SLOT(enableActionBox()));

    connect(m_connection, SIGNAL(dataSended(QString)), this, SLOT(showOutput(QString)));
    connect(m_connection, SIGNAL(dataReceived(QString)), this, SLOT(showInput(QString)));

    connect(m_connection, SIGNAL(printInputType(QString)), this, SLOT(showInput(QString)));
    connect(m_connection, SIGNAL(printOutputType(QString)), this, SLOT(showOutput(QString)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendRunCommand()
{
    m_connection->sendCommand(static_cast<uint>(Command::RUN));
}

void MainWindow::sendKillCommand()
{
    m_connection->sendCommand(static_cast<uint>(Command::KILL));
}

void MainWindow::sendNewName()
{
    QString name = ui->nameBox->text();
    // validate name

    m_connection->sendNewName(name);
}

void MainWindow::sendNewPassword()
{
    QString password = ui->passwordBox->text();
    // validate password

    m_connection->sendNewPassword(password);
}

void MainWindow::sendBinary()
{
    if (!m_binaryPath.isEmpty()) {
        m_connection->sendBinary(m_binaryPath);
    }
}

void MainWindow::chooseBinary()
{
    m_binaryPath = QFileDialog::getOpenFileName(this, tr("Choose file"));
}

void MainWindow::sendStatusRequest()
{
    m_connection->sendStatusRequest();
}

void MainWindow::enableActionBox()
{
    ui->actionBox->setEnabled(true);
    ui->statusBox->setEnabled(true);
    //m_inTerminal->clear();
    //m_outTerminal->clear();
}

void MainWindow::disableActionBox()
{
    ui->actionBox->setEnabled(false);
    ui->statusBox->setEnabled(false);
}

void MainWindow::showInput(QString data)
{
    m_inTerminal->putData(data);
}

void MainWindow::showOutput(QString data)
{
    m_outTerminal->putData(data);
}
