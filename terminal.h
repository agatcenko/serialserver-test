#ifndef TERMINAL_H
#define TERMINAL_H


#include <QPlainTextEdit>

class Terminal : public QPlainTextEdit
{
    Q_OBJECT

public:
    explicit Terminal(QWidget *parent = 0);

    void putData(const QString &data);
};

#endif // TERMINAL_H
