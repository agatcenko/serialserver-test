#include "crc.h"

#define POLY 0x1021
#define SEED 0xFFFF

unsigned short updateCRC(unsigned short acc, const unsigned char input)
{
    acc ^= (input << 8);

    for (unsigned char i = 0; i < 8; ++i) {
        if ((acc & 0x8000) == 0x8000) {
            acc <<= 1;
            acc ^= POLY;
        }
        else {
            acc <<= 1;
        }
    }

    return acc;
}

unsigned short calculateCRC(const unsigned char* data, const unsigned long long len)
{
    unsigned short crc = SEED;

    for (unsigned long long i = 0; i < len; ++i) {
        crc = updateCRC(crc, data[i]);
    }

    return crc;
}
