#include "terminal.h"

#include <QScrollBar>

Terminal::Terminal(QWidget *parent) : QPlainTextEdit(parent)
{

}

void Terminal::putData(const QString &data)
{
    insertPlainText(data);
    insertPlainText("\n");

    QScrollBar *bar = verticalScrollBar();
    bar->setValue(bar->maximum());
}
