#include "crc.h"
#include "serialconnection.h"

#include <QFile>
#include <QDebug>
#include <QVector>
#include <QFileInfo>

SerialConnection::SerialConnection()
{
    m_serial = new QSerialPort(this);
    m_settings = new SettingsDialog;

    m_responseTimer = new QTimer(this);
    m_responseTimer->setSingleShot(true);

    connect(m_serial, &QSerialPort::readyRead, this, &SerialConnection::readData);
    connect(m_responseTimer, SIGNAL(timeout()), this, SLOT(timeoutHandler()));
}

SerialConnection::~SerialConnection()
{

}

void SerialConnection::sendCommand(uchar cmd)
{
    sendPackage(OutputPackage::COMMAND, QByteArray(1, cmd));
}

void SerialConnection::sendBinary(QString path)
{
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Cannot open file " << path;
        return;
    }

    m_binary = file.readAll();
    m_binaryChunkCounter = 0;

    if (m_binary.isEmpty()) {
        qDebug() << "File is empty";
    }

    QFileInfo fileInfo(file.fileName());
    QString binaryName(fileInfo.fileName());
    ushort binaryCRC = calculateCRC((const unsigned char*)m_binary.data(), m_binary.size());

    sendBinaryPreamble(binaryName, binaryCRC);
}

void SerialConnection::sendBinaryEnd()
{
    sendPackage(OutputPackage::BINARY_END);

    emit stopBinarySend();
}

void SerialConnection::sendBinaryChunk()
{
    QByteArray binaryChunk;

    QByteArray chunk = m_binary.mid(m_binaryChunkCounter * m_chunkSize, m_chunkSize);
    while (chunk.size() < m_chunkSize) {
        chunk.append('0');
    }
    qDebug() << "chunk counter: " << m_binaryChunkCounter;
    addValToPackage(m_binaryChunkCounter++, binaryChunk);
    binaryChunk.append(chunk);

    sendPackage(OutputPackage::BINARY_CHUNK, binaryChunk);
}

void SerialConnection::sendBinaryPreamble(QString name, uint16_t binaryCRC)
{
    QByteArray binaryPreamble;

    uint8_t nameSize = name.size();

    addValToPackage(nameSize, binaryPreamble);
    for (auto i = 0; i < nameSize; ++i) {
        addValToPackage(name.at(i).toLatin1(), binaryPreamble);
    }
    addValToPackage(m_chunkSize, binaryPreamble);
    addValToPackage((unsigned long long)m_binary.size(), binaryPreamble);
    addValToPackage(binaryCRC, binaryPreamble);

    QString package = packageToString(binaryPreamble);
    emit startBinarySend();

    m_binaryState = BinarySendState::PREAMBLE;
    sendPackage(OutputPackage::BINARY_PREAMBLE, binaryPreamble);
}

void SerialConnection::sendNewName(QString name)
{
    sendPackage(OutputPackage::CHANGE_NAME, name.toUtf8());
}

void SerialConnection::sendNewPassword(QString password)
{
    sendPackage(OutputPackage::CHANGE_PASSWORD, password.toUtf8());
}

void SerialConnection::sendBluetoothMac()
{

}

void SerialConnection::sendStatusRequest()
{
    sendPackage(OutputPackage::STATUS_INFO);
}

void SerialConnection::openPort()
{
    SettingsDialog::Settings s = m_settings->settings();
    m_serial->setPortName(s.name);
    m_serial->setBaudRate(s.baudRate);
    m_serial->setDataBits(s.dataBits);
    m_serial->setParity(s.parity);
    m_serial->setStopBits(s.stopBits);
    m_serial->setFlowControl(s.flowControl);
    if (m_serial->open(QIODevice::ReadWrite)) {
        emit portOpened();
        qDebug() << "connect success";
    }
    else {
        qDebug() << "connect failed";
    }
}

void SerialConnection::closePort()
{
    if (m_serial->isOpen()) {
        m_serial->close();
        emit portClosed();
    }
}

bool SerialConnection::isValidInput() const
{
    for (auto i = 0; i < m_data.size() - 5; ++i) {
        if (m_data.at(i) == 0x7D && m_data.at(i + 1) == 0x7F) {
            size_t dataSize = (m_data.at(i + 4) << 8) + m_data.at(i + 3);
            if (m_data.at(7 + dataSize) == 0x7E) { // 7 - package info size
                return true;
            }
        }
    }

    return false;
}

void SerialConnection::readData()
{
    m_responseTimer->stop();

    m_data.append(m_serial->readAll());

    if (isValidInput()) {
        InputPackage type = InputPackage::UNKNOWN;
        int i = 0;
        for (i = 0; i < m_data.size() - 2; ++i) {
            if (m_data.at(i) == 0x7D && m_data.at(i + 1) == 0x7F) {
                type = static_cast<InputPackage>(m_data.at(i + 2));
                break;
            }
        }

        // parse package, calculate crc

        QString inTypeStr = inPackageName[type];
        if (type == InputPackage::RESULT && i + 5 < m_data.size()) {
            ReturnCode code = static_cast<ReturnCode>(m_data.at(i + 5));
            inTypeStr += ": " + returnCodeName[code];
        }

        emit printInputType(inTypeStr);
        emit dataReceived(packageToString(m_data));

        m_data.clear();

        checkBinarySendState();
    }
}

void SerialConnection::openSettings()
{
    m_settings->show();
}

void SerialConnection::writeData(const QByteArray &data)
{
    m_responseTimer->start(1000);
    m_serial->write(data);
}

QString SerialConnection::packageToString(const QByteArray &data)
{
    QString res;
    QString dataStr = QString(data.toHex()).toUpper();
    for (int i = 0; i < dataStr.size(); i += 2) {
        QString val = dataStr.mid(i, 2);
        QString hexVal;
        hexVal.append("0x");
        hexVal.append(val);
        hexVal.append(" ");
        res.append(hexVal);
    }

    return res;
}

void SerialConnection::sendPackage(OutputPackage type, const QByteArray &data)
{
    if (!m_serial->isOpen()) {
        qDebug() << "sendPackage failed, serial port is not opened";
        return;
    }

    QByteArray package;
    ushort dataSize = data.size();
    ushort packageSize = dataSize + 8;
    package.resize(packageSize);

    package[0] = 0x7D;
    package[1] = 0x7F;
    package[2] = static_cast<uchar>(type);
    package[3] = dataSize & 0xFF;
    package[4] = dataSize >> 8;
    package.replace(5, dataSize, data);
    // calculate crc
    package[packageSize - 1] = 0x7E;

    emit printOutputType(outPackageName[type]);
    emit dataSended(packageToString(package));

    writeData(package);
}

void SerialConnection::timeoutHandler()
{
    //checkBinarySendState();
    emit stopBinarySend();
}

void SerialConnection::checkBinarySendState()
{
    if (m_binaryState != BinarySendState::NONE) {
        switch (m_binaryState) {
        case BinarySendState::PREAMBLE:
            m_binaryState = BinarySendState::CHUNK;
            sendBinaryChunk();
            break;
        case BinarySendState::CHUNK:
            if (m_chunkSize * m_binaryChunkCounter >= m_binary.size()) {
                m_binaryState = BinarySendState::END;
                sendBinaryEnd();
            }
            else {
                sendBinaryChunk();
            }
            break;
        case BinarySendState::END:
            m_binaryState = BinarySendState::NONE;
            break;
        default:
            break;
        }
    }
}
