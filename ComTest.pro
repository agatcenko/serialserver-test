#-------------------------------------------------
#
# Project created by QtCreator 2016-04-27T13:39:47
#
#-------------------------------------------------

QT       += core gui serialport

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ComTest
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
        settingsdialog.cpp \
        terminal.cpp \
        serialconnection.cpp \
    crc.cpp

HEADERS  += mainwindow.h \
            settingsdialog.h \
            terminal.h \
            serialconnection.h \
            types.h \
    crc.h

FORMS    += mainwindow.ui \
            settingsdialog.ui
