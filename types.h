#ifndef TYPES_H
#define TYPES_H

#include <QMap>
#include <QString>

enum class OutputPackage : unsigned char
{
    COMMAND = 0x00,
    BINARY_PREAMBLE = 0x10,
    BINARY_CHUNK = 0x11,
    BINARY_END = 0x12,
    CHANGE_NAME = 0x20,
    CHANGE_PASSWORD = 0x21,
    BT_MAC = 0x30,
    STATUS_INFO = 0x40
};

enum class InputPackage : unsigned char
{
    STATUS_INFO = 0x70,
    RESULT = 0x80,
    UNKNOWN = 0xFF
};

enum class ReturnCode : unsigned char
{
    SUCCESS = 0x00,
    INCORRECT_NAME = 0x10,
    CHANGE_NAME_FAILED = 0x20,
    CHANGE_PASSWORD_FAILED = 0x30,
    CMD_EXEC_FAILED = 0x40,
    INCORRECT_CMD = 0x41,
    BINARY_CRC_FAILED = 0x50,
    UNKNOWN_COMMAND = 0xFF
};

static const QMap<OutputPackage, QString> outPackageName = {
    { OutputPackage::COMMAND, "Command msg:" },
    { OutputPackage::BINARY_PREAMBLE, "Binary preamble msg" },
    { OutputPackage::BINARY_CHUNK, "Binary chunk msg" },
    { OutputPackage::BINARY_END, "Binary end msg" },
    { OutputPackage::CHANGE_NAME, "Change name msg" },
    { OutputPackage::CHANGE_PASSWORD, "Change password msg" },
    { OutputPackage::BT_MAC, "Bluetooth MAC msg" },
    { OutputPackage::STATUS_INFO, "Status Info msg" }
};

static const QMap<InputPackage, QString> inPackageName = {
    { InputPackage::STATUS_INFO, "Status reply msg" },
    { InputPackage::RESULT, "Result code msg" },
    { InputPackage::UNKNOWN, "Unknown msg" }
};

static const QMap<ReturnCode, QString> returnCodeName = {
    { ReturnCode::SUCCESS, "Success" },
    { ReturnCode::INCORRECT_NAME, "Incorrect name" },
    { ReturnCode::CHANGE_NAME_FAILED, "Change name failed" },
    { ReturnCode::CHANGE_PASSWORD_FAILED, "Change password failed" },
    { ReturnCode::CMD_EXEC_FAILED, "Command execution failed" },
    { ReturnCode::INCORRECT_CMD, "Incorrect command" },
    { ReturnCode::BINARY_CRC_FAILED, "Binary crc verification failed" },
    { ReturnCode::UNKNOWN_COMMAND, "Unknown command" }
};

enum class Command : unsigned char
{
    KILL = 0x00,
    RUN = 0x01
};

#endif // TYPES_H
